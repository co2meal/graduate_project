#!/usr/bin/perl -w
######################################################################
#
# c2ksample.pl - A sample program for conv2kr.pm.
# Copyright (C) 2009 Younghong "Hong" Cho <hongcho at sori dot org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#*********************************************************************
#
# == History
# 2009-06-26 Created.
######################################################################
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use lib dirname(dirname abs_path $0) . "/lib";

use strict;
use conv2kr;

######################################################################
# Constants.

my $ICONV = "/usr/bin/iconv";
my $TMP_PREFIX = "/tmp/temp.r2k.";

######################################################################

# Initialize the library environment.
conv2kr::configure($ICONV, $TMP_PREFIX);

# Input string in UTF-8, which is the most common Korean character
# set encoding.

foreach(<STDIN>) {
	my $str_r = $_;

	$str_r =~ s/-/<>/g;

	# Convert the "MCT" result string back to Johab.
	#my ($str_n, $str_n2) = conv2kr::mct2Johab($str_r, 0);
	my ($str_n, $str_n2) = conv2kr::jamo2Johab($str_r, 0);
	$str_n =~ s/<>//g;
	# print("result\t: $str_n\n");
	# print("$str_n2\n");
	# print("Johab result2\t: $str_n2\n");
	my $str_u = conv2kr::johab2Utf8($str_n);
	print("$str_u\n");
}

######################################################################
# End.
exit(0);
######################################################################
