#!/usr/bin/perl -w
######################################################################
#
# c2ksample.pl - A sample program for conv2kr.pm.
# Copyright (C) 2009 Younghong "Hong" Cho <hongcho at sori dot org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#*********************************************************************
#
# == History
# 2009-06-26 Created.
######################################################################
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use lib dirname(dirname abs_path $0) . "/lib";

use strict;
use conv2kr;

######################################################################
# Constants.

my $ICONV = "/usr/bin/iconv";
my $TMP_PREFIX = "/tmp/temp.r2k.";

######################################################################

# Initialize the library environment.
conv2kr::configure($ICONV, $TMP_PREFIX);

# Input string in UTF-8, which is the most common Korean character
# set encoding.
foreach (<STDIN>) {
	my $str_i = $_;
	# Convert the UTF-8 string to Johab first.
	my $str_j = conv2kr::utf82Johab($str_i);
	# print("Johab Input\t: $str_j\n");

	# Convert the Johab input string to "MCT".
	# This is where the actual rules are applied.
	#my $str_r = conv2kr::johab2Mct($str_j);
	my $str_r = conv2kr::johab2Jamo($str_j);
	# print("MCT result\t: $str_r\n");
	print("$str_r\n");
}

######################################################################
# End.
exit(0);
######################################################################
