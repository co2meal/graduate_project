Experiment Setup
====

The task is to fill the blank in a sentence which is given as:
    #거 싸인 좀 싸인 좀부탁한다고,
    그# 싸인 좀 싸인 좀부탁한다고,
    그거 #인 좀 싸인 좀부탁한다고,
    그거 싸# 좀 싸인 좀부탁한다고,
    그거 싸인 # 싸인 좀부탁한다고,
    그거 싸인 좀 #인 좀부탁한다고,
    그거 싸인 좀 싸# 좀부탁한다고,
    그거 싸인 좀 싸인 #부탁한다고,
    그거 싸인 좀 싸인 좀#탁한다고,
    그거 싸인 좀 싸인 좀부#한다고,
    그거 싸인 좀 싸인 좀부탁#다고,
    그거 싸인 좀 싸인 좀부탁한#고,
    그거 싸인 좀 싸인 좀부탁한다#,
    ## 싸인 좀 싸인 좀부탁한다고,
    그거 ## 좀 싸인 좀부탁한다고,
    그거 싸인 좀 ## 좀부탁한다고,
    그거 싸인 좀 싸인 ##탁한다고,
    그거 싸인 좀 싸인 좀##한다고,
    그거 싸인 좀 싸인 좀부##다고,
    그거 싸인 좀 싸인 좀부탁##고,
    그거 싸인 좀 싸인 좀부탁한##,
    그거 싸인 좀 싸인 ###한다고,
    그거 싸인 좀 싸인 좀###다고,
    그거 싸인 좀 싸인 좀부###고,
    그거 싸인 좀 싸인 좀부탁###,
    그거 싸인 좀 싸인 ####다고,
    그거 싸인 좀 싸인 좀####고,
    그거 싸인 좀 싸인 좀부####,

Currently two kinds of model are experimented.
1. GRU seq2seq with HS inputs
2. 1 + Fine Tune
3. 2 + Attention mechanism


1. Simple Seq2Seq (Omitted since the result was invaluable)
2. Fine Tune
    * QA-hard
      * Steps: 23000
      * Perplexity: 2.4
      * Accuracy: 22.2%
      * Distance: 1.41
    * QA-easy
      * Steps: 18800
      * Perplexity: 1.0
      * Accuracy: 75%
      * Distance: 0.25
3. Attention mechanism
    * QA-hard
      * Steps: 96000
      * Perplexity: 2.0
      * Accuracy: 15.1%
      * Distance: 1.53
    * QA-easy
      * Steps: 23000
      * Perplexity: 1.0
      * Accuracy: 63.7%
      * Distance: 0.36


* QA-easy score decreased when attention mechanism was employed.
  I suspect the reason is that it focuses just one grapheme,
  which makes it hard to consider context and phonetic rules both.
  * We can observe this phenomena as confusion between -i/-ga
* I need to stop experimenting QA-hard, it's too difficult problem and long-time consuming.

Goal
====

* HS-HS (Done)
* HS-HS + Attention (Done)
* HC-HS
* HC-HS + Attention
* HCHS-HS
* **HCHS-HS + Attention**
* Hopefully the last performs best
