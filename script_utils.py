import os
import pipe
import subprocess

def pipe_simple_text_perl_script(simple_text, perl_script):
    """ It receives strings of UTF-8 """
    FNULL = open(os.devnull, "w")
    pipe = subprocess.Popen(["perl", perl_script], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=FNULL)
    # pipe = subprocess.Popen(["perl", perl_script], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    pipe.stdin.write(simple_text.encode("utf-8"))
    pipe.stdin.close()
    FNULL.close()
    return pipe.stdout.read().decode("utf-8").strip()

