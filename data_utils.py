# coding: utf-8

"""Utilities for downloading data from WMT, tokenizing, vocabularies."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import gzip
import os
import re
import tarfile
import subprocess
import korean

from six.moves import urllib

from tensorflow.python.platform import gfile
import tensorflow as tf

from konlpy.tag import Twitter
twitter = Twitter()

# Special vocabulary symbols - we always put them at the start.
_PAD = b"_PAD"
_GO = b"_GO"
_EOS = b"_EOS"
_UNK = b"_UNK"
_BLK = b"#"
_START_VOCAB = [_PAD, _GO, _EOS, _UNK, _BLK]

PAD_ID = 0
GO_ID = 1
EOS_ID = 2
UNK_ID = 3
BLK_ID = 4

# Regular expressions used to tokenize.
_WORD_SPLIT = re.compile(b"([.,!?~\-\"':;)(])")


def pipe_simple_text_perl_script(simple_text, perl_script):
        """ It receives strings of UTF-8 """
        FNULL = open(os.devnull, "w")
        pipe = subprocess.Popen(["perl", perl_script], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=FNULL)
        # pipe = subprocess.Popen(["perl", perl_script], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        pipe.stdin.write(simple_text.encode("utf-8"))
        pipe.stdin.close()
        FNULL.close()
        return pipe.stdout.read().decode("utf-8").strip()


def tokenizer_sp(sentence):
    """Very basic tokenizer: split the sentence into a list of tokens."""
    words = []
    for space_separated_fragment in sentence.strip().split():
        words.extend(_WORD_SPLIT.split(space_separated_fragment))
    return [w for w in words if w]


def tokenizer_mo(sentence):
    return ["/".join(t) for t in twitter.pos(sentence)]


def tokenizer_hc(sentence):
    """ """
    words = []
    for ch in list(sentence.strip().decode('utf-8')):
        words.append(ch.encode('utf-8'))
    return words


def tokenizer_hs(sentence):
    """"""
    words = []
    sentence = sentence.strip().decode('utf-8')
    for ch in sentence:
        if korean.hangul.is_hangul(ch):
            words.extend([j.encode('utf-8') for j in korean.hangul.split_char(ch) if j != ''])
        else:
            words.append(ch.encode('utf-8'))
    return words


def tokenizer_ra(sentence):
    """ """
    sentence = sentence.strip().decode('utf-8')
    # Replace - character to # because of the duplicate in the script.
    sentence.replace("-", "#")
    sentence = pipe_simple_text_perl_script(sentence, "./scripts/kr2roman.pl")

    return list(sentence)


def create_vocabulary(vocabulary_path, data_path, max_vocabulary_size, tokenizer):
    """Create vocabulary file (if it does not exist yet) from data file.
    Data file is assumed to contain one sentence per line. Each sentence is
    tokenized and digits are normalized (if normalize_digits is set).
    Vocabulary contains the most-frequent tokens up to max_vocabulary_size.
    We write it to vocabulary_path in a one-token-per-line format, so that later
    token in the first line gets id=0, second line gets id=1, and so on.
    Args:
        vocabulary_path: path where the vocabulary will be created.
        data_path: data file that will be used to create vocabulary.
        max_vocabulary_size: limit on the size of the created vocabulary.
        tokenizer: a function to use to tokenize each data sentence;
            if None, basic_tokenizer will be used.
        normalize_digits: Boolean; if true, all digits are replaced by 0s.
    """
    if not gfile.Exists(vocabulary_path):
        print("Creating vocabulary %s from data %s" % (vocabulary_path, data_path))
        vocab = {}
        with gfile.GFile(data_path, mode="rb") as f:
            counter = 0
            for line in f:
                counter += 1
                if counter % 1000 == 0:
                    print("    processing line %d" % counter)
                line = tf.compat.as_bytes(line)
                tokens = tokenizer(line)
                for w in tokens:
                    word = w
                    if word in vocab:
                        vocab[word] += 1
                    else:
                        vocab[word] = 1
            vocab_list = _START_VOCAB + sorted(vocab, key=vocab.get, reverse=True)
            if len(vocab_list) > max_vocabulary_size:
                vocab_list = vocab_list[:max_vocabulary_size]
            with gfile.GFile(vocabulary_path, mode="wb") as vocab_file:
                for w in vocab_list:
                    vocab_file.write(w + b"\n")


def initialize_vocabulary(vocabulary_path):
    """Initialize vocabulary from file.
    We assume the vocabulary is stored one-item-per-line, so a file:
        dog
        cat
    will result in a vocabulary {"dog": 0, "cat": 1}, and this function will
    also return the reversed-vocabulary ["dog", "cat"].
    Args:
        vocabulary_path: path to the file containing the vocabulary.
    Returns:
        a pair: the vocabulary (a dictionary mapping string to integers), and
        the reversed vocabulary (a list, which reverses the vocabulary mapping).
    Raises:
        ValueError: if the provided vocabulary_path does not exist.
    """
    if gfile.Exists(vocabulary_path):
        rev_vocab = []
        with gfile.GFile(vocabulary_path, mode="rb") as f:
            rev_vocab.extend(f.readlines())
        rev_vocab = [line.strip('\r\n') for line in rev_vocab]
        vocab = dict([(x, y) for (y, x) in enumerate(rev_vocab)])
        return vocab, rev_vocab
    else:
        raise ValueError("Vocabulary file %s not found.", vocabulary_path)


def sentence_to_token_ids(sentence, vocabulary,
                          tokenizer):
    """Convert a string to list of integers representing token-ids.
    For example, a sentence "I have a dog" may become tokenized into
    ["I", "have", "a", "dog"] and with vocabulary {"I": 1, "have": 2,
    "a": 4, "dog": 7"} this function will return [1, 2, 4, 7].
    Args:
        sentence: the sentence in bytes format to convert to token-ids.
        vocabulary: a dictionary mapping tokens to integers.
        tokenizer: a function to use to tokenize each sentence;
            if None, basic_tokenizer will be used.
        normalize_digits: Boolean; if true, all digits are replaced by 0s.
    Returns:
        a list of integers, the token-ids for the sentence.
    """

    words = tokenizer(sentence)
    # Normalize digits by 0 before looking words up in the vocabulary.
    return [vocabulary.get(w, UNK_ID) for w in words]


def data_to_token_ids(data_path, target_path, vocabulary_path,
                      tokenizer):
    """Tokenize data file and turn into token-ids using given vocabulary file.
    This function loads data line-by-line from data_path, calls the above
    sentence_to_token_ids, and saves the result to target_path. See comment
    for sentence_to_token_ids on the details of token-ids format.
    Args:
        data_path: path to the data file in one-sentence-per-line format.
        target_path: path where the file with token-ids will be created.
        vocabulary_path: path to the vocabulary file.
        tokenizer: a function to use to tokenize each sentence;
            if None, basic_tokenizer will be used.
        normalize_digits: Boolean; if true, all digits are replaced by 0s.
    """
    if not gfile.Exists(target_path):
        print("Tokenizing data in %s" % data_path)
        vocab, _ = initialize_vocabulary(vocabulary_path)
        with gfile.GFile(data_path, mode="rb") as data_file:
            with gfile.GFile(target_path, mode="w") as tokens_file:
                counter = 0
                for line in data_file:
                    counter += 1
                    if counter % 1000 == 0:
                        print("    tokenizing line %d" % counter)
                    token_ids = sentence_to_token_ids(line, vocab, tokenizer)
                    tokens_file.write(" ".join([str(tok) for tok in token_ids]) + "\n")


def get_tokenizer(code):
    if code == "sp":
        return tokenizer_sp
    elif code == "mo":
        return tokenizer_mo
    elif code == "hc":
        return tokenizer_hc
    elif code == "hs":
        return tokenizer_hs
    elif code == "ra":
        return tokenizer_ra
    return None


def is_answerable(answer):
    # answer is utf-8 text
    if re.search(r"\s", answer):
        return False
    if _WORD_SPLIT.search(answer):
        return False
    return True


def is_answerable_easy(answer):
    # answer is utf-8 text
    if answer in u"은 는 이 가 을 를".split():
        return True
    else:
        return False


def create_question_answer(data_path, quest_path, answer_path):
    if not gfile.Exists(quest_path) or not gfile.Exists(answer_path):
        print("Creating QA sets from data %s" % (data_path))
        with gfile.GFile(data_path, mode="rb") as f:
            with gfile.GFile(quest_path, mode="wb") as quest_file:
                with gfile.GFile(answer_path, mode="wb") as answer_file:
                    counter = 0
                    for line in f:
                        counter += 1
                        if counter % 1000 == 0:
                            print("    processing line %d" % counter)

                        line = line.decode('utf-8').strip()
                        linelen = len(line)
                        for size in [1, 2, 3, 4]:
                            for i in range(linelen - size + 1):
                                answer = line[i:(i + size)]
                                if is_answerable(answer):
                                    quest = line[:i] + (u"#" * size) + line[(i + size):]
                                    quest_file.write(quest.encode('utf-8') + '\n')
                                    answer_file.write(answer.encode('utf-8') + '\n')


def create_question_answer_easy(data_path, quest_path, answer_path):
    if not gfile.Exists(quest_path) or not gfile.Exists(answer_path):
        print("Creating QA sets from data %s" % (data_path))
        with gfile.GFile(data_path, mode="rb") as f:
            with gfile.GFile(quest_path, mode="wb") as quest_file:
                with gfile.GFile(answer_path, mode="wb") as answer_file:
                    counter = 0
                    for line in f:
                        counter += 1
                        if counter % 1000 == 0:
                            print("    processing line %d" % counter)

                        line = line.decode('utf-8').strip()
                        linelen = len(line)
                        for size in [1]:
                            for i in range(linelen - size + 1):
                                answer = line[i:(i + size)]
                                if is_answerable_easy(answer):
                                    quest = line[:i] + (u"#" * size) + line[(i + size):]
                                    quest_file.write(quest.encode('utf-8') + '\n')
                                    answer_file.write(answer.encode('utf-8') + '\n')


def create_nospace(data_path, nospace_path):
    if not gfile.Exists(nospace_path):
        print("Creating Nospace sets from data %s" % (data_path))
        with gfile.GFile(data_path, mode="rb") as f:
            with gfile.GFile(nospace_path, mode="wb") as nospace_file:
                counter = 0
                for line in f:
                    counter += 1
                    if counter % 1000 == 0:
                        print("    processing line %d" % counter)

                    line = line.decode('utf-8').strip()
                    nospace = line.replace(' ', '')
                    nospace_file.write(nospace.encode('utf-8') + '\n')


def prepare_data(data_dir, vocabulary_size, code):
    """Get WMT data into data_dir, create vocabularies and tokenize data.
    Args:
        data_dir: directory in which the data sets will be stored.
        en_vocabulary_size: size of the English vocabulary to create and use.
        fr_vocabulary_size: size of the French vocabulary to create and use.
        tokenizer: a function to use to tokenize each data sentence;
            if None, basic_tokenizer will be used.
    Returns:
        A tuple of 6 elements:
            (1) path to the token-ids for training data-set,
            (2) path to the token-ids for development data-set,
            (3) path to the vocabulary file,
    """

    tokenizer = get_tokenizer(code)

    # Get wmt data to the specified directory.
    train_path = os.path.join(data_dir, "train.txt")
    valid_path = os.path.join(data_dir, "valid.txt")

    # Create vocabularies of the appropriate sizes.
    vocab_path = os.path.join(data_dir, "vocab.%s" % code)
    create_vocabulary(vocab_path, train_path, vocabulary_size, tokenizer)

    # Create token ids for the training data.
    train_token_ids_path = os.path.join(data_dir, "train.ids.%s.txt" % code)
    data_to_token_ids(train_path, train_token_ids_path, vocab_path, tokenizer)

    # Create token ids for the development data.
    valid_token_ids_path = os.path.join(data_dir, "valid.ids.%s.txt" % code)
    data_to_token_ids(valid_path, valid_token_ids_path, vocab_path, tokenizer)

    return (train_token_ids_path, valid_token_ids_path, vocab_path)


def prepare_question_answer_data(data_dir, vocabulary_size, code):
    """Get WMT data into data_dir, create vocabularies and tokenize data.
    Args:
        data_dir: directory in which the data sets will be stored.
        en_vocabulary_size: size of the English vocabulary to create and use.
        fr_vocabulary_size: size of the French vocabulary to create and use.
        tokenizer: a function to use to tokenize each data sentence;
            if None, basic_tokenizer will be used.
    Returns:
        A tuple of 6 elements:
            (1) path to the token-ids for Question training data-set,
            (2) path to the token-ids for Answer training data-set,
            (3) path to the token-ids for Question development data-set,
            (4) path to the token-ids for Answer development data-set,
            (5) path to the vocabulary file,
    """

    tokenizer = get_tokenizer(code)

    # Get wmt data to the specified directory.
    train_path = os.path.join(data_dir, "train.txt")
    valid_path = os.path.join(data_dir, "valid.txt")

    # Create_question_answer for training data
    train_quest_path = os.path.join(data_dir, "train.quest.txt")
    train_answer_path = os.path.join(data_dir, "train.answer.txt")
    create_question_answer(train_path, train_quest_path, train_answer_path)

    # Create vocabularies of the appropriate sizes.
    vocab_path = os.path.join(data_dir, "vocab.%s" % code)
    create_vocabulary(vocab_path, train_path, vocabulary_size, tokenizer)

    # Create token ids for the training data.
    train_quest_token_ids_path = os.path.join(data_dir, "train.quest.ids.%s.txt" % code)
    train_answer_token_ids_path = os.path.join(data_dir, "train.answer.ids.%s.txt" % code)
    data_to_token_ids(train_quest_path, train_quest_token_ids_path, vocab_path, tokenizer)
    data_to_token_ids(train_answer_path, train_answer_token_ids_path, vocab_path, tokenizer)

    # Create_question_answer for development data.
    valid_quest_path = os.path.join(data_dir, "valid.quest.txt")
    valid_answer_path = os.path.join(data_dir, "valid.answer.txt")
    create_question_answer(valid_path, valid_quest_path, valid_answer_path)

    # Create token ids for the development data.
    valid_quest_token_ids_path = os.path.join(data_dir, "valid.quest.ids.%s.txt" % code)
    valid_answer_token_ids_path = os.path.join(data_dir, "valid.answer.ids.%s.txt" % code)
    data_to_token_ids(valid_quest_path, valid_quest_token_ids_path, vocab_path, tokenizer)
    data_to_token_ids(valid_answer_path, valid_answer_token_ids_path, vocab_path, tokenizer)

    return (train_quest_token_ids_path, train_answer_token_ids_path,
            valid_quest_token_ids_path, valid_answer_token_ids_path,
            vocab_path)


def prepare_question_answer_data(data_dir, vocabulary_size, code):
    """Get WMT data into data_dir, create vocabularies and tokenize data.
    Args:
        data_dir: directory in which the data sets will be stored.
        en_vocabulary_size: size of the English vocabulary to create and use.
        fr_vocabulary_size: size of the French vocabulary to create and use.
        tokenizer: a function to use to tokenize each data sentence;
            if None, basic_tokenizer will be used.
    Returns:
        A tuple of 6 elements:
            (1) path to the token-ids for Question training data-set,
            (2) path to the token-ids for Answer training data-set,
            (3) path to the token-ids for Question development data-set,
            (4) path to the token-ids for Answer development data-set,
            (5) path to the vocabulary file,
    """

    tokenizer = get_tokenizer(code)

    # Get wmt data to the specified directory.
    train_path = os.path.join(data_dir, "train.txt")
    valid_path = os.path.join(data_dir, "valid.txt")

    # Create_question_answer for training data
    train_quest_path = os.path.join(data_dir, "train.quest.easy.txt")
    train_answer_path = os.path.join(data_dir, "train.answer.easy.txt")
    create_question_answer_easy(train_path, train_quest_path, train_answer_path)

    # Create vocabularies of the appropriate sizes.
    vocab_path = os.path.join(data_dir, "vocab.%s" % code)
    create_vocabulary(vocab_path, train_path, vocabulary_size, tokenizer)

    # Create token ids for the training data.
    train_quest_token_ids_path = os.path.join(data_dir, "train.quest.easy.ids.%s.txt" % code)
    train_answer_token_ids_path = os.path.join(data_dir, "train.answer.easy.ids.%s.txt" % code)
    data_to_token_ids(train_quest_path, train_quest_token_ids_path, vocab_path, tokenizer)
    data_to_token_ids(train_answer_path, train_answer_token_ids_path, vocab_path, tokenizer)

    # Create_question_answer for development data.
    valid_quest_path = os.path.join(data_dir, "valid.quest.easy.txt")
    valid_answer_path = os.path.join(data_dir, "valid.answer.easy.txt")
    create_question_answer_easy(valid_path, valid_quest_path, valid_answer_path)

    # Create token ids for the development data.
    valid_quest_token_ids_path = os.path.join(data_dir, "valid.quest.easy.ids.%s.txt" % code)
    valid_answer_token_ids_path = os.path.join(data_dir, "valid.answer.easy.ids.%s.txt" % code)
    data_to_token_ids(valid_quest_path, valid_quest_token_ids_path, vocab_path, tokenizer)
    data_to_token_ids(valid_answer_path, valid_answer_token_ids_path, vocab_path, tokenizer)

    return (train_quest_token_ids_path, train_answer_token_ids_path,
            valid_quest_token_ids_path, valid_answer_token_ids_path,
            vocab_path)


def prepare_question_answer_data_easy(data_dir, vocabulary_size, code):
    """Get WMT data into data_dir, create vocabularies and tokenize data.
    Args:
        data_dir: directory in which the data sets will be stored.
        en_vocabulary_size: size of the English vocabulary to create and use.
        fr_vocabulary_size: size of the French vocabulary to create and use.
        tokenizer: a function to use to tokenize each data sentence;
            if None, basic_tokenizer will be used.
    Returns:
        A tuple of 6 elements:
            (1) path to the token-ids for Question training data-set,
            (2) path to the token-ids for Answer training data-set,
            (3) path to the token-ids for Question development data-set,
            (4) path to the token-ids for Answer development data-set,
            (5) path to the vocabulary file,
    """

    tokenizer = get_tokenizer(code)

    # Get wmt data to the specified directory.
    train_path = os.path.join(data_dir, "train.txt")
    valid_path = os.path.join(data_dir, "valid.txt")

    # Create_question_answer for training data
    train_quest_path = os.path.join(data_dir, "train.quest.easy.txt")
    train_answer_path = os.path.join(data_dir, "train.answer.easy.txt")
    create_question_answer_easy(train_path, train_quest_path, train_answer_path)

    # Create vocabularies of the appropriate sizes.
    vocab_path = os.path.join(data_dir, "vocab.%s" % code)
    create_vocabulary(vocab_path, train_path, vocabulary_size, tokenizer)

    # Create token ids for the training data.
    train_quest_token_ids_path = os.path.join(data_dir, "train.quest.easy.ids.%s.txt" % code)
    train_answer_token_ids_path = os.path.join(data_dir, "train.answer.easy.ids.%s.txt" % code)
    data_to_token_ids(train_quest_path, train_quest_token_ids_path, vocab_path, tokenizer)
    data_to_token_ids(train_answer_path, train_answer_token_ids_path, vocab_path, tokenizer)

    # Create_question_answer for development data.
    valid_quest_path = os.path.join(data_dir, "valid.quest.easy.txt")
    valid_answer_path = os.path.join(data_dir, "valid.answer.easy.txt")
    create_question_answer_easy(valid_path, valid_quest_path, valid_answer_path)

    # Create token ids for the development data.
    valid_quest_token_ids_path = os.path.join(data_dir, "valid.quest.easy.ids.%s.txt" % code)
    valid_answer_token_ids_path = os.path.join(data_dir, "valid.answer.easy.ids.%s.txt" % code)
    data_to_token_ids(valid_quest_path, valid_quest_token_ids_path, vocab_path, tokenizer)
    data_to_token_ids(valid_answer_path, valid_answer_token_ids_path, vocab_path, tokenizer)

    return (train_quest_token_ids_path, train_answer_token_ids_path,
            valid_quest_token_ids_path, valid_answer_token_ids_path,
            vocab_path)


def prepare_nospace_data(data_dir, vocabulary_size, code):
    """Get WMT data into data_dir, create vocabularies and tokenize data.
    Args:
        data_dir: directory in which the data sets will be stored.
        en_vocabulary_size: size of the English vocabulary to create and use.
        fr_vocabulary_size: size of the French vocabulary to create and use.
        tokenizer: a function to use to tokenize each data sentence;
            if None, basic_tokenizer will be used.
    Returns:
        A tuple of 6 elements:
            (1) path to the token-ids for Question training data-set,
            (2) path to the token-ids for Answer training data-set,
            (3) path to the token-ids for Question development data-set,
            (4) path to the token-ids for Answer development data-set,
            (5) path to the vocabulary file,
    """

    tokenizer = get_tokenizer(code)

    # Get wmt data to the specified directory.
    train_path = os.path.join(data_dir, "train.txt")
    valid_path = os.path.join(data_dir, "valid.txt")

    # Create nospace data for training data
    train_nospace_path = os.path.join(data_dir, "train.nospace.txt")
    create_nospace(train_path, train_nospace_path)

    # Create vocabularies of the appropriate sizes.
    vocab_path = os.path.join(data_dir, "vocab.%s" % code)
    create_vocabulary(vocab_path, train_path, vocabulary_size, tokenizer)

    # Create token ids for the training data.
    train_nospace_token_ids_path = os.path.join(data_dir, "train.nospace.ids.%s.txt" % code)
    train_original_token_ids_path = os.path.join(data_dir, "train.original.ids.%s.txt" % code)
    data_to_token_ids(train_nospace_path, train_nospace_token_ids_path, vocab_path, tokenizer)
    data_to_token_ids(train_path, train_original_token_ids_path, vocab_path, tokenizer)

    # Create_question_answer for development data.
    valid_nospace_path = os.path.join(data_dir, "valid.nospace.txt")
    create_nospace(valid_path, valid_nospace_path)

    # Create token ids for the development data.
    valid_nospace_token_ids_path = os.path.join(data_dir, "valid.nospace.ids.%s.txt" % code)
    valid_original_token_ids_path = os.path.join(data_dir, "valid.original.ids.%s.txt" % code)
    data_to_token_ids(valid_nospace_path, valid_nospace_token_ids_path, vocab_path, tokenizer)
    data_to_token_ids(valid_path, valid_original_token_ids_path, vocab_path, tokenizer)

    return (train_nospace_token_ids_path, train_original_token_ids_path,
            valid_nospace_token_ids_path, valid_original_token_ids_path,
            vocab_path)


def prepare_corrector_data(data_dir, vocabulary_size, code):
    """Get WMT data into data_dir, create vocabularies and tokenize data.
    Args:
        data_dir: directory in which the data sets will be stored.
        en_vocabulary_size: size of the English vocabulary to create and use.
        fr_vocabulary_size: size of the French vocabulary to create and use.
        tokenizer: a function to use to tokenize each data sentence;
            if None, basic_tokenizer will be used.
    Returns:
        A tuple of 6 elements:
            (1) path to the token-ids for Question training data-set,
            (2) path to the token-ids for Answer training data-set,
            (3) path to the token-ids for Question development data-set,
            (4) path to the token-ids for Answer development data-set,
            (5) path to the vocabulary file,
    """

    tokenizer = get_tokenizer(code)

    # Get wmt data to the specified directory.
    train_source_path = os.path.join(data_dir, "train.corrector.source.txt")
    train_target_path = os.path.join(data_dir, "train.corrector.target.txt")
    valid_source_path = os.path.join(data_dir, "valid.corrector.source.txt")
    valid_target_path = os.path.join(data_dir, "valid.corrector.target.txt")

    # Create vocabularies of the appropriate sizes.
    vocab_path = os.path.join(data_dir, "vocab.corrector.%s" % code)
    create_vocabulary(vocab_path, train_source_path, vocabulary_size, tokenizer)

    # Create token ids for the training data.
    train_source_token_ids_path = os.path.join(data_dir, "train.corrector.source.ids.%s.txt" % code)
    train_target_token_ids_path = os.path.join(data_dir, "train.corrector.target.ids.%s.txt" % code)
    data_to_token_ids(train_source_path, train_source_token_ids_path, vocab_path, tokenizer)
    data_to_token_ids(train_target_path, train_target_token_ids_path, vocab_path, tokenizer)

    # Create token ids for the development data.
    valid_source_token_ids_path = os.path.join(data_dir, "valid.corrector.source.ids.%s.txt" % code)
    valid_target_token_ids_path = os.path.join(data_dir, "valid.corrector.target.ids.%s.txt" % code)
    data_to_token_ids(valid_source_path, valid_source_token_ids_path, vocab_path, tokenizer)
    data_to_token_ids(valid_target_path, valid_target_token_ids_path, vocab_path, tokenizer)

    return (train_source_token_ids_path, train_target_token_ids_path,
            valid_source_token_ids_path, valid_target_token_ids_path,
            vocab_path)
