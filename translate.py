"""Binary for training translation models and decoding from them.
Running this program without --decode will download the WMT corpus into
the directory specified as --data_dir and tokenize it in a very basic way,
and then start training a model saving checkpoints to --train_dir.
Running with --decode starts an interactive loop so you can see how
the current checkpoint translates English sentences into French.
See the following papers for more information on neural translation models.
 * http://arxiv.org/abs/1409.3215
 * http://arxiv.org/abs/1409.0473
 * http://arxiv.org/abs/1412.2007
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math
import os
import random
import sys
import time

import numpy as np
from six.moves import xrange    # pylint: disable=redefined-builtin
import tensorflow as tf

import data_utils
import seq2seq_model

tf.app.flags.DEFINE_float("learning_rate", 0.5, "Learning rate.")
tf.app.flags.DEFINE_float("learning_rate_decay_factor", 0.99,
                          "Learning rate decays by this much.")
tf.app.flags.DEFINE_float("max_gradient_norm", 5.0,
                          "Clip gradients to this norm.")
tf.app.flags.DEFINE_integer("batch_size", 64,
                            "Batch size to use during training.")
tf.app.flags.DEFINE_integer("size", 200, "Size of each model layer.")
tf.app.flags.DEFINE_integer("embedding_size", 16, "Size of embedding layer.")
tf.app.flags.DEFINE_integer("num_layers", 1, "Number of layers in the model.")
tf.app.flags.DEFINE_string("source_code", "hs", "Code of source encoding")
tf.app.flags.DEFINE_string("target_code", "hs", "Code of target encoding")
tf.app.flags.DEFINE_string("data_dir", "/tmp", "Data directory")
tf.app.flags.DEFINE_string("train_dir", "/tmp", "Training directory.")
tf.app.flags.DEFINE_string("output_path", "output.txt",
                           "File for the result of run_valid mode")
tf.app.flags.DEFINE_string("load_pretrain_model", None, "...")
tf.app.flags.DEFINE_integer("max_train_data_size", 0,
                            "Limit on the size of training data (0: no limit)")
tf.app.flags.DEFINE_integer("steps_per_checkpoint", 200,
                            "How many training steps to do per checkpoint.")

tf.app.flags.DEFINE_integer("beam_size", 10,
                            "How many training steps to do per checkpoint.")
tf.app.flags.DEFINE_boolean("beam_search", False,
                            "Set to True for beam_search.")
tf.app.flags.DEFINE_boolean("attention", False,
                            "Set to True for attention mechanism for decoder.")
tf.app.flags.DEFINE_boolean("train", False,
                            "Set to True for training.")
tf.app.flags.DEFINE_boolean("pretrain", False,
                            "Set to True for pretraining.")
tf.app.flags.DEFINE_boolean("decode", False,
                            "Set to True for interactive decoding.")
tf.app.flags.DEFINE_boolean("run_valid", False,
                            "Set to True for decoding development set.")
tf.app.flags.DEFINE_boolean("self_test", False,
                            "Run a self-test if this is set to True.")
tf.app.flags.DEFINE_boolean("use_fp16", False,
                            "Train using fp16 instead of fp32.")

tf.app.flags.DEFINE_boolean("easy_quest_answer", False,
                            "Easy question only.")
tf.app.flags.DEFINE_boolean("nospace", False,
                            "Nospace task.")
tf.app.flags.DEFINE_boolean("corrector", False,
                            "Prepare corrector dataset.")

FLAGS = tf.app.flags.FLAGS

sess_config = tf.ConfigProto()
sess_config.allow_soft_placement = True
sess_config.gpu_options.allow_growth = True
# sess_config.gpu_options.per_process_gpu_memory_fraction = 0.4

# We use a number of buckets and pad to the closest one for efficiency.
# See seq2seq_model.Seq2SeqModel for details of how they work.
# _buckets = [(5, 10), (10, 15), (20, 25), (40, 50), (60, 70), (80, 90), (100, 110)]


def get_buckets():
    if FLAGS.target_code == 'hs' or FLAGS.target_code == 'ra':
        #buckets = [(10, 10), (20, 10), (30, 10), (40, 20), (50, 20), (60, 20), (70, 20)]
        buckets = [(10, 10), (20, 20), (40, 40), (80, 80)]
    else:
        buckets = [(5, 5), (10, 10), (20, 20), (40, 40), (60, 60)]
    return buckets

_buckets = get_buckets()


def get_prepare():
    if FLAGS.easy_quest_answer:
        return data_utils.prepare_question_answer_data_easy
    elif FLAGS.nospace:
        return data_utils.prepare_nospace_data
    elif FLAGS.corrector:
        return data_utils.prepare_corrector_data
    else:
        raise NotImplementedError
        return data_utils.prepare_question_answer_data

def get_vocab_size(code):
    if code == 'hs':
        return 100
    elif code == 'hc':
        return 1000
    raise 'No'


def read_data(source_path, target_path, max_size=None, use_bucket=True):
    """Read data from source and target files and put into buckets.
    Args:
        source_path: path to the files with token-ids for the source language.
        target_path: path to the file with token-ids for the target language;
            it must be aligned with the source file: n-th line contains the desired
            output for n-th line from the source_path.
        max_size: maximum number of lines to read, all other will be ignored;
            if 0 or None, data files will be read completely (no limit).
    Returns:
        data_set: a list of length len(_buckets); data_set[n] contains a list of
            (source, target) pairs read from the provided data files that fit
            into the n-th bucket, i.e., such that len(source) < _buckets[n][0] and
            len(target) < _buckets[n][1]; source and target are lists of token-ids.
    """
    if use_bucket:
        data_set = [[] for _ in _buckets]
    else:
        data_set = []
    with tf.gfile.GFile(source_path, mode="r") as source_file:
        with tf.gfile.GFile(target_path, mode="r") as target_file:
            source, target = source_file.readline(), target_file.readline()
            counter = 0
            while source and target and (not max_size or counter < max_size):
                counter += 1
                if counter % 100000 == 0:
                    print("    reading data line %d" % counter)
                    sys.stdout.flush()
                source_ids = [int(x) for x in source.split()]
                target_ids = [int(x) for x in target.split()]
                target_ids.append(data_utils.EOS_ID)


                if use_bucket:
                    for bucket_id, (source_size, target_size) in enumerate(_buckets):
                        if len(source_ids) < source_size and len(target_ids) < target_size:
                            data_set[bucket_id].append([source_ids, target_ids])
                            break
                else:
                    data_set.append([source_ids, target_ids])

                source, target = source_file.readline(), target_file.readline()
    return data_set


def create_model(session, forward_only, beam_search, beam_size=10, attention=False):
    """Create translation model and initialize or load parameters in session."""
    dtype = tf.float16 if FLAGS.use_fp16 else tf.float32
    model = seq2seq_model.Seq2SeqModel(
            get_vocab_size(FLAGS.source_code),
            get_vocab_size(FLAGS.target_code),
            _buckets,
            FLAGS.size,
            FLAGS.embedding_size,
            FLAGS.num_layers,
            FLAGS.max_gradient_norm,
            FLAGS.batch_size,
            FLAGS.learning_rate,
            FLAGS.learning_rate_decay_factor,
            forward_only=forward_only,
            beam_search=beam_search,
            beam_size=beam_size,
            attention=attention,
            dtype=dtype)

    ckpt = tf.train.get_checkpoint_state(FLAGS.train_dir)
    if ckpt and tf.gfile.Exists(ckpt.model_checkpoint_path):
        print("Reading model parameters from %s" % ckpt.model_checkpoint_path)
        model.saver.restore(session, ckpt.model_checkpoint_path)
    else:
        print("Created model with fresh parameters.")
        session.run(tf.initialize_all_variables())
    return model

def pretrain():
    """Pretrain the model with reconstruction dataset."""
    # Prepare WMT data.
    print("Preparing data in %s" % FLAGS.data_dir)
    s_train, s_valid, _ = data_utils.prepare_data(
            FLAGS.data_dir, get_vocab_size(FLAGS.source_code), FLAGS.source_code)
    t_train, t_valid, _ = data_utils.prepare_data(
            FLAGS.data_dir, get_vocab_size(FLAGS.target_code), FLAGS.target_code)

    # Beam search is false during training operation and usedat inference .
    beam_search = False
    beam_size = 10
    attention = FLAGS.attention

    print(FLAGS)

    with tf.Session(config=sess_config) as sess:
        # Create model.
        print("Creating %d layers of %d units." % (FLAGS.num_layers, FLAGS.size))
        model = create_model(sess, False, beam_search=beam_search,
                             beam_size=beam_size, attention=attention)

        # Read data into buckets and compute their sizes.
        print ("Reading development and training data (limit: %d)."
               % FLAGS.max_train_data_size)
        valid_set = read_data(s_valid, t_valid)
        train_set = read_data(s_train, t_train, FLAGS.max_train_data_size)
        train_bucket_sizes = [len(train_set[b]) for b in xrange(len(_buckets))]
        train_total_size = float(sum(train_bucket_sizes))

        # A bucket scale is a list of increasing numbers from 0 to 1 that we'll use
        # to select a bucket. Length of [scale[i], scale[i+1]] is proportional to
        # the size if i-th training bucket, as used later.
        train_buckets_scale = [sum(train_bucket_sizes[:i + 1]) / train_total_size
                                                     for i in xrange(len(train_bucket_sizes))]

        # This is the training loop.
        step_time, loss = 0.0, 0.0
        current_step = 0
        previous_losses = []
        while True:
            # Choose a bucket according to data distribution. We pick a random number
            # in [0, 1] and use the corresponding interval in train_buckets_scale.
            random_number_01 = np.random.random_sample()
            bucket_id = min([i for i in xrange(len(train_buckets_scale))
                                             if train_buckets_scale[i] > random_number_01])

            # Get a batch and make a step.
            start_time = time.time()
            encoder_inputs, decoder_inputs, target_weights = model.get_batch(
                    train_set, bucket_id)
            _, step_loss, _ = model.step(sess, encoder_inputs, decoder_inputs,
                                         target_weights, bucket_id, False, beam_search)
            step_time += (time.time() - start_time) / FLAGS.steps_per_checkpoint
            loss += step_loss / FLAGS.steps_per_checkpoint
            current_step += 1

            # Once in a while, we save checkpoint, print statistics, and run evals.
            if current_step % FLAGS.steps_per_checkpoint == 0:
                # Print statistics for the previous epoch.
                perplexity = math.exp(float(loss)) if loss < 300 else float("inf")
                print ("global step %d learning rate %.4f step-time %.2f perplexity "
                             "%.2f" % (model.global_step.eval(), model.learning_rate.eval(),
                                                 step_time, perplexity))
                # Decrease learning rate if no improvement was seen over last 3 times.
                if len(previous_losses) > 2 and loss > max(previous_losses[-3:]):
                    sess.run(model.learning_rate_decay_op)
                previous_losses.append(loss)
                # Save checkpoint and zero timer and loss.
                checkpoint_path = os.path.join(FLAGS.train_dir, "translate.ckpt.%s" % FLAGS.target_code)
                model.saver.save(sess, checkpoint_path, global_step=model.global_step)
                step_time, loss = 0.0, 0.0
                # Run evals on development set and print their perplexity.
                for bucket_id in xrange(len(_buckets)):
                    if len(valid_set[bucket_id]) == 0:
                        print("    eval: empty bucket %d" % (bucket_id))
                        continue
                    encoder_inputs, decoder_inputs, target_weights = model.get_batch(
                            valid_set, bucket_id)
                    _, eval_loss, _ = model.step(sess, encoder_inputs, decoder_inputs,
                                                 target_weights, bucket_id, True, beam_search)
                    eval_ppx = math.exp(float(eval_loss)) if eval_loss < 300 else float(
                            "inf")
                    print("    eval: bucket %d perplexity %.2f" % (bucket_id, eval_ppx))
                sys.stdout.flush()


def load_pretrain_model(pretrain_dir, beam_search, beam_size, attention):
    old_train_dir = FLAGS.__dict__['__flags']['train_dir']
    FLAGS.__dict__['__flags']['train_dir'] = pretrain_dir
    with tf.Session(config=sess_config) as sess:
        model = create_model(sess, False, beam_search=beam_search, beam_size=beam_size, attention=attention)
        keys = [v.name for v in tf.get_collection('trainable_variables')
                if v.name.startswith('embedding_rnn_seq2seq/RNN') or
                v.name.startswith('embedding_attention_seq2seq/embedding_attention_decoder/attention_decoder/Att')]
        values = sess.run([sess.graph.get_tensor_by_name(k) for k in keys])
    tf.reset_default_graph()
    FLAGS.__dict__['__flags']['train_dir'] = old_train_dir
    return keys, values


def train():
    """Train a en->fr translation model using WMT data."""
    # Prepare WMT data.
    print("Preparing data in %s" % FLAGS.data_dir)
    prepare = get_prepare()
    s_train, _, s_valid, _, _ = prepare(FLAGS.data_dir,
                                        get_vocab_size(FLAGS.source_code),
                                        FLAGS.source_code)
    _, t_train, _, t_valid, _ = prepare(FLAGS.data_dir,
                                        get_vocab_size(FLAGS.target_code),
                                        FLAGS.target_code)

    # Beam search is false during training operation and usedat inference .
    beam_search = False
    beam_size = 10
    attention = FLAGS.attention

    print(FLAGS)

    if FLAGS.load_pretrain_model:
        keys, values = load_pretrain_model(FLAGS.load_pretrain_model, beam_search, beam_size, attention)

    with tf.Session(config=sess_config) as sess:
        # Create model.
        print("Creating %d layers of %d units." % (FLAGS.num_layers, FLAGS.size))
        model = create_model(sess, False, beam_search=beam_search, beam_size=beam_size, attention=attention)

        if FLAGS.load_pretrain_model:
            print("Copy encoder variables from pretrained data")
            for key, value in zip(keys, values):
                ref = sess.graph.get_tensor_by_name(key)
                sess.run(tf.assign(ref, value))

        # Read data into buckets and compute their sizes.
        print ("Reading development and training data (limit: %d)."
               % FLAGS.max_train_data_size)
        valid_set = read_data(s_valid, t_valid)
        train_set = read_data(s_train, t_train, FLAGS.max_train_data_size)
        train_bucket_sizes = [len(train_set[b]) for b in xrange(len(_buckets))]
        train_total_size = float(sum(train_bucket_sizes))

        # A bucket scale is a list of increasing numbers from 0 to 1 that we'll use
        # to select a bucket. Length of [scale[i], scale[i+1]] is proportional to
        # the size if i-th training bucket, as used later.
        train_buckets_scale = [sum(train_bucket_sizes[:i + 1]) / train_total_size
                               for i in xrange(len(train_bucket_sizes))]

        # This is the training loop.
        step_time, loss = 0.0, 0.0
        current_step = 0
        previous_losses = []
        while True:
            # Choose a bucket according to data distribution. We pick a random number
            # in [0, 1] and use the corresponding interval in train_buckets_scale.
            random_number_01 = np.random.random_sample()
            bucket_id = min([i for i in xrange(len(train_buckets_scale))
                                             if train_buckets_scale[i] > random_number_01])

            # Get a batch and make a step.
            start_time = time.time()
            encoder_inputs, decoder_inputs, target_weights = model.get_batch(
                    train_set, bucket_id)
            _, step_loss, _ = model.step(sess, encoder_inputs, decoder_inputs,
                                                                     target_weights, bucket_id, False, beam_search)
            step_time += (time.time() - start_time) / FLAGS.steps_per_checkpoint
            loss += step_loss / FLAGS.steps_per_checkpoint
            current_step += 1

            # Once in a while, we save checkpoint, print statistics, and run evals.
            if current_step % FLAGS.steps_per_checkpoint == 0:
                # Print statistics for the previous epoch.
                perplexity = math.exp(float(loss)) if loss < 300 else float("inf")
                print ("global step %d learning rate %.4f step-time %.2f perplexity "
                             "%.2f" % (model.global_step.eval(), model.learning_rate.eval(),
                                                 step_time, perplexity))
                # Decrease learning rate if no improvement was seen over last 3 times.
                if len(previous_losses) > 2 and loss > max(previous_losses[-3:]):
                    sess.run(model.learning_rate_decay_op)
                previous_losses.append(loss)
                # Save checkpoint and zero timer and loss.
                checkpoint_path = os.path.join(FLAGS.train_dir, "translate.ckpt.%s" % FLAGS.target_code)
                model.saver.save(sess, checkpoint_path, global_step=model.global_step)
                step_time, loss = 0.0, 0.0
                # Run evals on development set and print their perplexity.
                for bucket_id in xrange(len(_buckets)):
                    if len(valid_set[bucket_id]) == 0:
                        print("    eval: empty bucket %d" % (bucket_id))
                        continue
                    encoder_inputs, decoder_inputs, target_weights = model.get_batch(
                            valid_set, bucket_id)
                    _, eval_loss, _ = model.step(sess, encoder_inputs, decoder_inputs,
                                                                             target_weights, bucket_id, True, beam_search)
                    eval_ppx = math.exp(float(eval_loss)) if eval_loss < 300 else float(
                            "inf")
                    print("    eval: bucket %d perplexity %.2f" % (bucket_id, eval_ppx))
                sys.stdout.flush()


def decode():
    _buckets.append((160, 160))

    with tf.Session(config=sess_config) as sess:
        # Create model and load parameters.
        beam_size = FLAGS.beam_size
        beam_search = FLAGS.beam_search
        attention = FLAGS.attention
        model = create_model(sess, True, beam_search=beam_search, beam_size=beam_size, attention=attention)
        model.batch_size = 1    # We decode one sentence at a time.

        # Load vocabularies.
        s_vocab_path = os.path.join(FLAGS.data_dir, "vocab.%s" % FLAGS.source_code)
        t_vocab_path = os.path.join(FLAGS.data_dir, "vocab.%s" % FLAGS.target_code)
        s_vocab, _ = data_utils.initialize_vocabulary(s_vocab_path)
        _, rev_t_vocab = data_utils.initialize_vocabulary(t_vocab_path)

        tokenizer = data_utils.get_tokenizer(FLAGS.source_code)

        # Decode from standard input.
        if beam_search:
            sys.stdout.write("> ")
            sys.stdout.flush()
            sentence = sys.stdin.readline()
            while sentence:
                # Get token-ids for the input sentence.
                token_ids = data_utils.sentence_to_token_ids(tf.compat.as_bytes(sentence), s_vocab, tokenizer)
                # Which bucket does it belong to?
                bucket_id = min([b for b in xrange(len(_buckets))
                                 if _buckets[b][0] > len(token_ids)])
                # Get a 1-element batch to feed the sentence to the model.
                encoder_inputs, decoder_inputs, target_weights = model.get_batch(
                        {bucket_id: [(token_ids, [])]}, bucket_id)
                # Get output logits for the sentence.
                # print bucket_id
                path, symbol, output_logits = model.step(sess, encoder_inputs, decoder_inputs,
                                                         target_weights, bucket_id, True, beam_search )

                k = output_logits[0]
                paths = []
                for kk in range(beam_size):
                    paths.append([])
                curr = range(beam_size)
                num_steps = len(path)
                for i in range(num_steps-1, -1, -1):
                    for kk in range(beam_size):
                        paths[kk].append(symbol[i][curr[kk]])
                        curr[kk] = path[i][curr[kk]]
                recos = set()
                print("Replies --------------------------------------->")
                for kk in range(beam_size):
                    foutputs = [int(logit) for logit in paths[kk][::-1]]
            # If there is an EOS symbol in outputs, cut them at that point.
                    if data_utils.EOS_ID in foutputs:
                        foutputs = foutputs[:foutputs.index(data_utils.EOS_ID)]
                    rec = " ".join([tf.compat.as_str(rev_t_vocab[output]) for output in foutputs])
                    if rec not in recos:
                        recos.add(rec)
                        print(rec)

                print("> ", "")
                sys.stdout.flush()
                sentence = sys.stdin.readline()
        else:
            sys.stdout.write("> ")
            sys.stdout.flush()
            sentence = sys.stdin.readline()

            while sentence:
                # Get token-ids for the input sentence.
                token_ids = data_utils.sentence_to_token_ids(tf.compat.as_bytes(sentence), s_vocab, tokenizer)
                # Which bucket does it belong to?
                bucket_id = min([b for b in xrange(len(_buckets))
                                                 if _buckets[b][0] > len(token_ids)])
                # for loc in locs:
                        # Get a 1-element batch to feed the sentence to the model.
                encoder_inputs, decoder_inputs, target_weights = model.get_batch(
                                {bucket_id: [(token_ids, [],)]}, bucket_id)

                _, _, output_logits = model.step(sess, encoder_inputs, decoder_inputs,
                                                 target_weights, bucket_id, True, beam_search)
                # This is a greedy decoder - outputs are just argmaxes of output_logits.

                outputs = [int(np.argmax(logit, axis=1)) for logit in output_logits]
                # If there is an EOS symbol in outputs, cut them at that point.
                if data_utils.EOS_ID in outputs:
                        # print outputs
                        outputs = outputs[:outputs.index(data_utils.EOS_ID)]

                print(" ".join([tf.compat.as_str(rev_t_vocab[output]) for output in outputs]))
                print("> ", "")
                sys.stdout.flush()
                sentence = sys.stdin.readline()

def run_valid():
    _buckets.append((160, 160))
    if FLAGS.source_code == 'hc':
        del _buckets[:]
        _buckets.append((160, 160))
        pass
    else:
        _buckets.append((160, 160))
    # _buckets = [(160, 160)]
    """"""
    prepare = get_prepare()
    print("Preparing data in %s" % FLAGS.data_dir)
    s_train, _, s_valid, _, _ = prepare(FLAGS.data_dir,
                                        get_vocab_size(FLAGS.source_code),
                                        FLAGS.source_code)
    _, t_train, _, t_valid, _ = prepare(FLAGS.data_dir,
                                        get_vocab_size(FLAGS.target_code),
                                        FLAGS.target_code)

    token_ids_list = []
    decoded = []

    # Read source ids
    print("Reading source file")
    with tf.gfile.GFile(s_valid, mode="r") as source_file:
        source = source_file.readline()
        while source:
            source_ids = [int(x) for x in source.split()]
            token_ids_list.append(source_ids)
            source = source_file.readline()

    # print("Reading source file")
    # valid_set = read_data(s_valid, t_valid, use_bucket=False)
    # token_ids_list, _ = zip(*valid_set)


    print("Running")
    with tf.Session(config=sess_config) as sess:
        # Create model and load parameters.
        beam_size = FLAGS.beam_size
        beam_search = FLAGS.beam_search
        attention = FLAGS.attention
        model = create_model(sess, True, beam_search=beam_search, beam_size=beam_size, attention=attention)
        #model = create_model(sess, True, beam_search=False)
        model.batch_size = 1    # We decode one sentence at a time.

        # Load vocabularies.
        t_vocab_path = os.path.join(FLAGS.data_dir, "vocab.corrector.%s" % FLAGS.target_code)
        _, rev_t_vocab = data_utils.initialize_vocabulary(t_vocab_path)

        # Replace space to other character if it has
        if any(v == ' ' for v in rev_t_vocab):
            rev_t_vocab[rev_t_vocab.index(' ')] = '_'

        tokenizer = data_utils.get_tokenizer(FLAGS.target_code)

        # Run evals on development set and print their perplexity.
        for token_ids in token_ids_list:
            bucket_id = min([np.inf] + [b for b in xrange(len(_buckets))
                            if _buckets[b][0] > len(token_ids)])
            # if bucket_id is np.inf:
            #     continue

            # Get a 1-element batch to feed the sentence to the model.
            encoder_inputs, decoder_inputs, target_weights = model.get_batch(
                    {bucket_id: [(token_ids, [])]}, bucket_id)
            # Get output logits for the sentence.
            _, _, output_logits = model.step(sess, encoder_inputs, decoder_inputs,
                                             target_weights, bucket_id, True, beam_search)
            # This is a greedy decoder - outputs are just argmaxes of output_logits.
            outputs = [int(np.argmax(logit, axis=1)) for logit in output_logits]
            # If there is an EOS symbol in outputs, cut them at that point.
            if data_utils.EOS_ID in outputs:
                outputs = outputs[:outputs.index(data_utils.EOS_ID)]

            decoded.append(" ".join([tf.compat.as_str(rev_t_vocab[output]) for output in outputs]))

    print("Writing")
    with tf.gfile.GFile(FLAGS.output_path, mode="wb") as output_file:
        for sentence in decoded:
            output_file.write(sentence + b"\n")


def self_test():
    """Test the translation model."""
    with tf.Session(config=sess_config) as sess:
        print("Self-test for neural translation model.")
        # Create model with vocabularies of 10, 2 small buckets, 2 layers of 32.
        model = seq2seq_model.Seq2SeqModel(10, 10, [(3, 3), (6, 6)], 32, 2,
                                           5.0, 32, 0.3, 0.99, num_samples=8)
        sess.run(tf.initialize_all_variables())

        # Fake data set for both the (3, 3) and (6, 6) bucket.
        data_set = ([([1, 1], [2, 2]), ([3, 3], [4]), ([5], [6])],
                    [([1, 1, 1, 1, 1], [2, 2, 2, 2, 2]), ([3, 3, 3], [5, 6])])
        for _ in xrange(5):    # Train the fake model for 5 steps.
            bucket_id = random.choice([0, 1])
            encoder_inputs, decoder_inputs, target_weights = model.get_batch(
                    data_set, bucket_id)
            model.step(sess, encoder_inputs, decoder_inputs, target_weights,
                                 bucket_id, False)


def main(_):
    if FLAGS.self_test:
        self_test()
    elif FLAGS.decode:
        decode()
    elif FLAGS.run_valid:
        run_valid()
    elif FLAGS.pretrain:
        pretrain()
    elif FLAGS.train:
        train()
    else:
        print("Specify a flag: train, pretrain, run_valid, decode, self_test")

if __name__ == "__main__":
    tf.app.run()
