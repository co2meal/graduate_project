# coding: utf-8

import numpy as np
import tensorflow as tf

import editdistance
import korean

import sklearn.metrics

import script_utils

tf.app.flags.DEFINE_string("code", "hs",
                           "Code of the text")
tf.app.flags.DEFINE_string("output_path", "output.txt",
                           "The path for output path")

FLAGS = tf.app.flags.FLAGS


def parse_groundtruth():
    sentences = []
    with tf.gfile.GFile("data/valid.corrector.target.txt", "rb") as f:
    # with tf.gfile.GFile("data/valid.answer.easy.txt", "rb") as f:
        for line in f:
            sentences.append(line.strip())
    return sentences


def parse_sp():
    sentences = []
    with tf.gfile.GFile("output/output.txt.sp", "rb") as f:
        for line in f:
            line = line.strip()
            line = " ".join(line.split())
            line = line.replace("_UNK", "@")
            line = line.replace(" .", ".")
            line = line.replace(" ?", "?")
            line = line.replace(" :", ":")
            line = line.replace(" !", "!")
            sentences.append(line)
    return sentences


def parse_hc():
    sentences = []
    with tf.gfile.GFile("output/output.txt.hc", "rb") as f:
        for line in f:
            line = line.strip()
            line = "".join(line.split())
            line = line.replace("_UNK", "@")
            line = line.replace("_", " ")
            sentences.append(line)
    return sentences


def parse_hs():
    sentences = []
    with tf.gfile.GFile(FLAGS.output_path, "rb") as f:
        for line in f:
            line = line.strip().decode('utf-8')
            line = "".join(line.split())
            line = line.replace("_UNK", "@")
            line = line.replace("_", " ")

            buf = []
            sentence = ""
            for ch in line:
                if len(buf) == 0:
                    if korean.hangul.is_initial(ch):
                        buf.append(ch)
                    else:
                        sentence += ch
                elif len(buf) == 1:
                    if korean.hangul.is_vowel(ch):
                        buf.append(ch)
                    else:
                        # Error
                        del buf[:]
                        sentence += u"@"
                elif len(buf) == 2:
                    if korean.hangul.is_final(ch):
                        buf.append(ch)
                    elif korean.hangul.is_initial(ch):
                        buf.append(u'')
                        sentence += korean.hangul.join_char(buf)
                        del buf[:]
                        buf.append(ch)
                    else:
                        buf.append(u'')
                        sentence += korean.hangul.join_char(buf)
                        del buf[:]
                        sentence += ch
                elif len(buf) == 3:
                    if korean.hangul.is_vowel(ch) and korean.hangul.is_initial(buf[2]):
                        ch_initial = buf[2]
                        buf[2] = u''
                        sentence += korean.hangul.join_char(buf)
                        del buf[:]
                        buf.append(ch_initial)
                        buf.append(ch)
                    else:
                        sentence += korean.hangul.join_char(buf)
                        del buf[:]
                        if korean.hangul.is_initial(ch):
                            buf.append(ch)
                        else:
                            sentence += ch
            if len(buf) > 0:
                while len(buf) < 3:
                    buf.append(u'')

                sentence += korean.hangul.join_char(buf)
                del buf[:]

            sentences.append(sentence.encode('utf-8'))

    return sentences


def parse_ra():
    with tf.gfile.GFile("output/output.txt.ra", "rb") as f:
        simple_text = ""
        for line in f:
            line = line.strip()
            line = "".join(line.split())
            line = line.replace("_UNK", "@")
            line = line.replace("_", " ")
            simple_text += line + "\n"

    simple_text = simple_text.decode('utf-8')
    simple_text = script_utils.pipe_simple_text_perl_script(simple_text, "./scripts/roman2kr.pl")
    return [line.encode('utf-8') for line in simple_text.splitlines()]


def get_predicted():
    if FLAGS.code == "hc":
        return parse_hc()
    elif FLAGS.code == "hs":
        return parse_hs()
    elif FLAGS.code == "ra":
        return parse_ra()

    return None


def evaluate(groundtruth, predicted):
    dists = []
    accs = []
    for g, p in zip(groundtruth, predicted):
        print(len(g.decode('utf-8')))
        print(g)
        print(p)
        print(editdistance.eval(g.decode('utf-8'), p.decode('utf-8')))
        dists.append(editdistance.eval(g.decode('utf-8'), p.decode('utf-8')))
        accs.append(g.decode('utf-8') == p.decode('utf-8'))
    return {'dist': np.mean(dists), 'acc': np.mean(accs)}


def evaluate_use0(groundtruth, predicted):
    dists = []
    accs = []
    for g, p in zip(groundtruth, predicted):
        g = g.decode('utf-8')
        p = p.decode('utf-8')[:1]
        g = "".join(korean.hangul.split_char(g))[0]
        if p and korean.hangul.is_hangul(p):
            p = "".join(korean.hangul.split_char(p))[0]
        print(g)
        print(p)
        print(editdistance.eval(g, p))
        dists.append(editdistance.eval(g, p))
        accs.append(g == p)
    return {'dist': np.mean(dists), 'acc': np.mean(accs)}


def josa_evaluate(groundtruth, predicted):
    print (sklearn.metrics.classification_report(
            groundtruth, predicted, labels="은 는 이 가 을 를".split()))
    print (sklearn.metrics.confusion_matrix(
            groundtruth, predicted, labels="은 는 이 가 을 를".split()))


def main(_):
    groundtruth = parse_groundtruth()

    predicted = get_predicted()
    print(evaluate(groundtruth, predicted))
#    print(josa_evaluate(groundtruth, predicted))


if __name__ == "__main__":
    tf.app.run()
